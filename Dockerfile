FROM registry.csworks.in/reguser/containers/debian:stable-slim
COPY http_srv /usr/bin
EXPOSE 8081
CMD ["http_srv"]
